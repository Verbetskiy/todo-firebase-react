import React from "react";
import PropTypes from "prop-types";

function Card({ cards, setShow, setCardId }) {
  return (
    <>
      {cards.map((card) => (
        <div key={card.id} className="card">
          <div className="card__title_container">
            <h2> {card.title}</h2>
            <div>
              <button
                type="button"
                className="card__button"
                onClick={() => {
                  setShow(2);
                  setCardId(card.id);
                }}
              >
                Edit
              </button>
              <button
                className="card__button"
                onClick={() => {
                  setShow(3);
                  setCardId(card.id);
                }}
              >
                Delete
              </button>
            </div>
          </div>
          <p>{card.description}</p>
        </div>
      ))}
    </>
  );
}

Card.propTypes = {
  cards: PropTypes.array.isRequired,
};

export default Card;
