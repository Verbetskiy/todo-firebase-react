import React, { useEffect, useState } from "react";
import { Redirect } from "react-router";
import Card from "./Card";
import firebase from "./firebase.js";
import CreateCard from "./CreateCard";
import Edit from "./Edit";
import Delete from "./Delete";

function Main() {
  const [cards, setCards] = useState([]);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(0);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [cardId, setCardId] = useState();

  function logOut() {
    localStorage.removeItem("email");
    window.location = "/";
  }

  useEffect(() => {
    const getCards = () => {
      setLoading(true);
      firebase
        .firestore()
        .collection("cards")
        .onSnapshot((querySnapshot) => {
          const items = [];
          querySnapshot.forEach((doc) => {
            items.push(doc.data());
          });
          setCards(items);
          setLoading(false);
        });
    };
    getCards();
  }, []);

  if (show === 1) {
    return (
      <CreateCard
        title={title}
        description={description}
        setTitle={setTitle}
        setDescription={setDescription}
        show={show}
        setShow={setShow}
      />
    );
  } else if (show === 2) {
    return (
      <Edit
        title={title}
        description={description}
        setTitle={setTitle}
        setDescription={setDescription}
        show={show}
        setShow={setShow}
        cardId={cardId}
      />
    );
  }else if (show === 3){
    return(
      <Delete setShow={setShow} cardId={cardId} />
    )
  }
  if (localStorage.getItem("email") === null) {
    return <Redirect to="/" />;
  }

  if (loading) {
    return <h1>Loading...</h1>;
  }

  let email = localStorage.getItem("email");

  return (
    <div className="main_body">
      <header>
        <div>
          <p id="show_email">{email}</p>
        </div>
        <button
          className="modalLogOut"
          type="button"
          onClick={() => {
            logOut();
          }}
        >
          <p>LogOut</p>
        </button>
      </header>
      <div className="main__container">
        <div className="card_container">
          <div className="card_template" onClick={() => setShow(1)}>
            <p>Push to create a card</p>
          </div>
          <Card cards={cards} setShow={setShow} setCardId={setCardId} />
        </div>
      </div>
    </div>
  );
}

export default Main;
