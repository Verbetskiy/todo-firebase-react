import React, { useState } from "react";
import PropTypes from "prop-types";

function LoginModal({ show, setShow }) {
  const [value, setValue] = useState("");
  const [error, setError] = useState("");

  function handleUserInput(e) {
    const val = e.target.value;
    setValue(val);
  }

  function validateEmail(e) {
    e.preventDefault();
    if (value.match(/^\w+([/.-]?\w+)*@\w+([/.-]?\w+)*(\.\w{2,3})+$/)) {
      localStorage.clear();
      localStorage.setItem("email", value);
      window.location = "/mainpage";
    } else {
      setError("Incorrect email.Try input your email again");
    }
  }

  return (
    <div className="loginModalSpace">
      <div
        className="loginModal_clickOutSide"
        onClick={() => {
          setShow(!show);
        }}
      ></div>
      <form className="loginModal" onSubmit={(e) => validateEmail(e)}>
        <label>
          <div className="cycle"></div>
          Email:
          <br />
          <input
            type="email"
            className="loginModalInput"
            placeholder="input email"
            value={value}
            onChange={handleUserInput}
          />
        </label>
        {error && <div className="loginModalError">{error}</div>}
        <br />
        <button type="submit" value="submit" className="loginModalSubmit">
          Submit
        </button>
      </form>
    </div>
  );
}

LoginModal.propTypes = {
  show: PropTypes.bool.isRequired,
};

export default LoginModal;
