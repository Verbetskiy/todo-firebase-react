import React from "react";
import PropTypes from "prop-types";
import firebase from "./firebase.js";

function Delete({ setShow, cardId }) {
  function del(card) {
    firebase
      .firestore()
      .collection("cards")
      .doc(card.id)
      .delete()
      .then((prev) => {
        prev.filter((element) => element.id !== card.id);
      })
      .catch((err) => {
        console.error(err);
      });
  }
  return (
    <div className="loginModalSpace">
      <div className="loginModal_clickOutSide" onClick={() => setShow(0)}></div>
      <div className="createModal">
        <h1>Delete note?</h1>
        <button
          className="createButton"
          onClick={() => del({ id: cardId }, setShow(0))}
        >
          Yes
        </button>
        <button className="createButton" onClick={() => del(setShow(0))}>
          no
        </button>
      </div>
    </div>
  );
}
Delete.propTypes = {
 
};

export default Delete;
