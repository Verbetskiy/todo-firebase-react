import React from "react";
import PropTypes from "prop-types";
import { v4 as uuidv4 } from "uuid";
import firebase from "./firebase.js";

function Create({ title, description, setTitle, setDescription, setShow }) {
  function addCard(newCard) {
    firebase
      .firestore()
      .collection("cards")
      .doc(newCard.id)
      .set(newCard)
      .catch((err) => {
        console.error(err);
      });
  }
  return (
    <div className="loginModalSpace">
      <div className="loginModal_clickOutSide" onClick={() => setShow(0)}></div>
      <div className="createModal">
        <h1>Create your note</h1>
        <div>
          <input
            type="text"
            className="createTitle"
            maxLength="20"
            placeholder="title max num of symb 20"
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <textarea
          className="createDescription"
          maxLength="100"
          wrap="hard"
          cols="10"
          rows="10"
          placeholder="description max num of symb 100"
          onChange={(e) => setDescription(e.target.value)}
        />
        <button
          className="createButton"
          onClick={() => addCard({ title, description, id: uuidv4() }, setShow(0))}
        >
          Create Note
        </button>
      </div>
    </div>
  );
}

Create.propTypes = {
  setTitle: PropTypes.func.isRequired,
  setDescription: PropTypes.func.isRequired,
  setShow: PropTypes.func.isRequired,
};

export default Create;
