import React from "react";
import PropTypes from "prop-types";
import firebase from "./firebase.js";

function Edit({
  title,
  description,
  setTitle,
  setDescription,
  setShow,
  cardId,
}) {
  function editCard(updatedCard) {
    firebase
      .firestore()
      .collection("cards")
      .doc(updatedCard.id)
      .update(updatedCard)
      .catch((err) => {
        console.error(err);
      });
  }
  return (
    <div className="loginModalSpace">
      <div className="loginModal_clickOutSide" onClick={() => setShow(0)}></div>
      <div className="createModal">
        <h1>Edit your note</h1>
        <div>
          <input
            type="text"
            className="createTitle"
            maxLength="20"
            placeholder="max lenght 20symb"
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <textarea
          className="createDescription"
          maxLength="100"
          wrap="hard"
          cols="10"
          rows="10"
          placeholder={(description, "max num of symb 100")}
          onChange={(e) => setDescription(e.target.value)}
        />
        <button
          className="createButton"
          onClick={() =>
            editCard({ title, description, id: cardId }, setShow(0))
          }
        >
          Submit
        </button>
      </div>
    </div>
  );
}
Edit.propTypes = {
  setTitle: PropTypes.func.isRequired,
  setDescription: PropTypes.func.isRequired,
  setShow: PropTypes.func.isRequired,
};

export default Edit;
