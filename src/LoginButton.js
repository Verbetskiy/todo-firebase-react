import React, { useState } from "react";
import LoginModal from "./LoginModal";

function LoginButton() {
  const [show, setShow] = useState(false);

  if (show) {
    return <LoginModal show={show} setShow={setShow} />;
  }

  return (
    <div className="logincont">
      <button
        className="login_button"
        onClick={() => {
          setShow(!show);
        }}
      >
        Press to Login
      </button>
    </div>
  );
}

export default LoginButton;
