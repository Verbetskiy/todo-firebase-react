import React from "react";
import LoginButton from "./LoginButton";
import Main from "./Main";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/mainpage" component={Main} />
        <Route exact path="/" component={LoginButton} />
      </Switch>
    </Router>
  );
}

export default App;
